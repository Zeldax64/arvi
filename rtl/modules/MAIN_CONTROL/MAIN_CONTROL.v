`timescale 1ns / 1ps

//OPCodes
`define OP_R_TYPE 		7'b0110011
`define OP_I_TYPE 		7'b0010011
`define OP_I_L_TYPE		7'b0000011
`define OP_S_TYPE 		7'b0100011
`define OP_B_TYPE 		7'b1100011
`define OP_U_TYPE_LUI 	7'b0110111
`define OP_U_TYPE_AUIPC 7'b0010111
`define OP_J_TYPE_JAL	7'b1101111
`define OP_J_TYPE_JALR	7'b1100111

module MAIN_CONTROL(
    output reg o_Branch,
    output reg o_MemRead,
    output reg o_MemWrite,
    output reg o_MemToReg,
    output reg [2:0] o_ALUOp,
    output reg [1:0] o_ALUSrcA,
    output reg o_ALUSrcB,
    output reg o_RegWrite,
    output reg [1:0] o_Jump,
    output reg o_PCplus4,
    input [6:0] i_OPCode
    );

	always @(*) begin

		case(i_OPCode)
			`OP_R_TYPE : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 0;
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 0;
				o_RegWrite = 1;
				o_ALUOp    = 3'b010;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_I_TYPE : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 0;
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 1;
				o_RegWrite = 1;
				o_ALUOp    = 3'b011;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_I_L_TYPE : begin
				o_Branch   = 0;
				o_MemRead  = 1;
				o_MemWrite = 0;
				o_MemToReg = 1;
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 1;
				o_RegWrite = 1;
				o_ALUOp    = 3'b000;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_S_TYPE : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 1;
				o_MemToReg = 1'bx; //Don't Care
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 1;
				o_RegWrite = 0; 
				o_ALUOp    = 3'b000;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_B_TYPE : begin
				o_Branch   = 1;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 1'bx; //Don't Care
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 0;
				o_RegWrite = 0; 
				o_ALUOp    = 3'b001;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_U_TYPE_LUI : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 0;
				o_ALUSrcA  = 2;
				o_ALUSrcB  = 1;
				o_RegWrite = 1;
				o_ALUOp    = 3'b100;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_U_TYPE_AUIPC : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 0;
				o_ALUSrcA  = 1;
				o_ALUSrcB  = 1;
				o_RegWrite = 1;
				o_ALUOp    = 3'b100;
    			o_Jump 	   = 0;
    			o_PCplus4  = 0;
			end

			`OP_J_TYPE_JAL : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 1'bx;
				o_ALUSrcA  = 2'bx;
				o_ALUSrcB  = 1'bx;
				o_RegWrite = 1;
				o_ALUOp    = 3'bxxx; // Don't use ALU
    			o_Jump 	   = 1;
    			o_PCplus4  = 1;
			end

			`OP_J_TYPE_JALR : begin
				o_Branch   = 0;
				o_MemRead  = 0;
				o_MemWrite = 0;
				o_MemToReg = 1'bx;
				o_ALUSrcA  = 0;
				o_ALUSrcB  = 1;
				o_RegWrite = 1;
				o_ALUOp    = 3'b100; // Use ALU as ADD due to U-Type (Check ALU_CONTROL)
    			o_Jump 	   = 2;
    			o_PCplus4  = 1;
			end

			default : begin
				o_Branch   = 1'bx;
				o_MemRead  = 1'bx;
				o_MemWrite = 1'bx;
				o_MemToReg = 1'bx; //Don't Care
				o_ALUSrcA  = 2'bx;
				o_ALUSrcB  = 1'bx;
				o_RegWrite = 1'bx; 
				o_ALUOp    = 2'bx;
    			o_Jump 	   = 1'bx;
    			o_PCplus4  = 1'bx;
			end

		endcase
	end

endmodule
