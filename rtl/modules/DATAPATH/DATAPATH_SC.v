/*
	File containing a DATAPATH module to a single-cycle RISC-V processor.
*/

`timescale 1ns / 1ps

`include "defines.vh"

module DATAPATH_SC(
	// Instruction Memory connections
	input  [`WORD_SIZE:0] i_IM_Instr,
	output [`WORD_SIZE:0] o_IM_Addr,
	
	// Data Memory connections
	input  [`WORD_SIZE:0] i_DM_ReadData,
	output [`WORD_SIZE:0] o_DM_Wd,
	output [`WORD_SIZE:0] o_DM_Addr,
	output o_DM_Wen,

	// Main Control connections
	input  i_MC_Branch,
	input  i_MC_MemRead,
	input  i_MC_MemWrite,
	input  i_MC_MemtoReg,
	input  [2:0] i_MC_ALUOp,
	input  [1:0] i_MC_ALUSrcA,
	input  i_MC_ALUSrcB,
	input  i_MC_RegWrite,
	input  [1:0] i_MC_Jump,
	input  i_MC_PCplus4,
	output [6:0] o_MC_OPCode,

	// Clock
	input i_clk
	);

	reg  [`WORD_SIZE:0] PC;
	reg [`WORD_SIZE:0] PC_next;

	// Instruction wires renaming
	wire [`WORD_SIZE:0] instr = i_IM_Instr;
	wire [6:0] opcode = instr[6:0];
	wire [2:0] f3     = instr[14:12];
	wire [6:0] f7     = instr[31:25];

	// REGISTER_FILE 
	wire [`WORD_SIZE:0] i_Wd;
	
	wire [`WORD_SIZE:0] Rd1;
	wire [`WORD_SIZE:0] Rd2;

	// IMM_GEN
	wire [`WORD_SIZE:0] Imm;

	// ALU
	wire [3:0] alu_control_lines;
	wire [`WORD_SIZE:0] A;
	wire [`WORD_SIZE:0] B;
	wire [`WORD_SIZE:0] Alu_Res;

	// Flags
	wire Z;

	// BRANCH_CONTROL
	wire DoBranch;

	// DATA MEMORY
	wire [`WORD_SIZE:0] DM_Addr;
	wire [`WORD_SIZE:0] DM_ReadData;

	// MAIN CONTROL renaming
	wire Branch        = i_MC_Branch;
	wire MemRead       = i_MC_MemRead;
	wire MemtoReg      = i_MC_MemtoReg;
	wire [2:0] ALUOp   = i_MC_ALUOp;
	wire MemWrite      = i_MC_MemWrite;
	wire [1:0] ALUSrcA = i_MC_ALUSrcA;
	wire ALUSrcB       = i_MC_ALUSrcB;
	wire RegWrite      = i_MC_RegWrite;
	wire [1:0] Jump    = i_MC_Jump;
	wire PCplus4 	   = i_MC_PCplus4;

	assign o_MC_OPCode = opcode;
	
	/*
		Initializing PC to 0;
	*/
	initial begin
		PC = 0;
	end

	/*
		Assigning PC
	*/
	always@(posedge i_clk) begin
		PC <= PC_next;
	end

	// Assigning instruction to be fetched
	assign o_IM_Addr = PC;

	REGISTER_FILE reg_file (
    	.o_Rd1(Rd1), 
    	.o_Rd2(Rd2), 
    	.i_Rnum1(instr[19:15]), 
    	.i_Rnum2(instr[24:20]), 
    	.i_Wen(RegWrite), 
    	.i_Wnum(instr[11:7]), 
    	.i_Wd(i_Wd), 
    	.i_clk(i_clk)
    );

	IMM_GEN  imm_gen (
		.i_Instr(instr),
		.o_Ext(Imm)
	);

	ALU_CONTROL alu_control (
		.o_ALUControlLines (alu_control_lines),
		.i_Funct7          (f7),
		.i_Funct3          (f3),
		.i_ALUOp           (ALUOp)
	);

	ALU alu (
		.i_op(alu_control_lines),
		.i_Ra(A),
		.i_Rb(B),
		.o_Z(Z),
		.o_Rc(Alu_Res)
	);

	BRANCH_CONTROL branch_control (
		.i_Branch   (Branch),
		.i_Z        (Z),
		.i_Res      (Alu_Res[0]),
		.i_f3       (f3),
		.o_DoBranch (DoBranch)
	);

	assign DM_Addr = Alu_Res[`WORD_SIZE:0];

/*	
	Data Memory signals
*/
	assign DM_ReadData = i_DM_ReadData;
	assign o_DM_Wd     = Rd2;
	assign o_DM_Addr   = DM_Addr;
	assign o_DM_Wen    = MemWrite;


	/*----- Datapath Muxes -----*/
	// PC Mux 
	// NOTA: this code and jump/branch signals must be improved
	//assign PC_next = (DoBranch | Jump) ? (PC + $signed(Imm<<1)) : PC + 4;
	always@(*) begin
		if(Jump[1]) begin // JALR
			PC_next = Alu_Res & 32'hFFFF_FFFE;
		end
		else
			if(Jump[0] || DoBranch) begin // JAL
			 	PC_next = PC + $signed(Imm<<1);
			end 
			else begin
				PC_next = PC + 4;
			end 
	end

	// ALU input A Mux
	assign A = ALUSrcA[1] ? 0  : 
			  (ALUSrcA[0] ? PC : Rd1);

	// ALU input B Mux
	assign B = ALUSrcB ? Imm : Rd2;

	// MemtoReg + PCplus4 Mux
	assign i_Wd = PCplus4  ? PC+4 :
				 (MemtoReg ? DM_ReadData : Alu_Res[`WORD_SIZE:0]);


endmodule